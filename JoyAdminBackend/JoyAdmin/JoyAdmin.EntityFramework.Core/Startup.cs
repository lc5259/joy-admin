﻿using Furion;
using Furion.DatabaseAccessor;
using Microsoft.Extensions.DependencyInjection;

namespace JoyAdmin.EntityFramework.Core
{
    public class Startup : AppStartup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDatabaseAccessor(options =>
            {
                options.CustomizeMultiTenants();
                options.AddDbPool<DefaultDbContext>(DbProvider.SqlServer);
            }, "JoyAdmin.Database.Migrations");
        }
    }
}