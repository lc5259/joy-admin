USE [db_joyadmin]
GO
/****** Object:  Table [dbo].[IPLog]    Script Date: 2022/7/9 15:32:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IPLog](
	[Id] [bigint] NOT NULL,
	[Ip] [nvarchar](50) NULL,
	[RealIp] [nvarchar](50) NULL,
	[UserAgent] [nvarchar](200) NULL,
	[Referer] [nvarchar](200) NULL,
	[UserId] [bigint] NOT NULL,
	[UserAccount] [nvarchar](50) NULL,
	[UserName] [nvarchar](50) NULL,
	[CreatedTime] [datetime2](7) NOT NULL,
	[CreatedUserId] [bigint] NOT NULL,
	[CreatedUser] [nvarchar](50) NULL,
	[ModifiedTime] [datetime2](7) NULL,
	[ModifiedUserId] [bigint] NOT NULL,
	[ModifiedUser] [nvarchar](50) NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_IPLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 2022/7/9 15:32:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[Id] [bigint] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Remark] [nvarchar](200) NULL,
	[Sort] [int] NOT NULL,
	[CreatedTime] [datetime2](7) NOT NULL,
	[CreatedUserId] [bigint] NOT NULL,
	[CreatedUser] [nvarchar](50) NULL,
	[ModifiedTime] [datetime2](7) NULL,
	[ModifiedUserId] [bigint] NOT NULL,
	[ModifiedUser] [nvarchar](50) NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoleSecurity]    Script Date: 2022/7/9 15:32:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleSecurity](
	[RoleId] [bigint] NOT NULL,
	[SecurityId] [bigint] NOT NULL,
 CONSTRAINT [PK_RoleSecurity] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC,
	[SecurityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Security]    Script Date: 2022/7/9 15:32:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Security](
	[Id] [bigint] NOT NULL,
	[UniqueCode] [nvarchar](50) NULL,
	[UniqueName] [nvarchar](50) NULL,
	[Sort] [int] NOT NULL,
	[CreatedTime] [datetime2](7) NOT NULL,
	[CreatedUserId] [bigint] NOT NULL,
	[CreatedUser] [nvarchar](50) NULL,
	[ModifiedTime] [datetime2](7) NULL,
	[ModifiedUserId] [bigint] NOT NULL,
	[ModifiedUser] [nvarchar](50) NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Security] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 2022/7/9 15:32:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [bigint] NOT NULL,
	[Account] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[Name] [nvarchar](100) NULL,
	[Phone] [nvarchar](50) NULL,
	[LinkPost] [nvarchar](50) NULL,
	[Remark] [nvarchar](500) NULL,
	[CreatedTime] [datetime2](7) NOT NULL,
	[CreatedUserId] [bigint] NOT NULL,
	[CreatedUser] [nvarchar](50) NULL,
	[ModifiedTime] [datetime2](7) NULL,
	[ModifiedUserId] [bigint] NOT NULL,
	[ModifiedUser] [nvarchar](50) NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 2022/7/9 15:32:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[UserId] [bigint] NOT NULL,
	[RoleId] [bigint] NOT NULL,
 CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Role] ([Id], [Name], [Remark], [Sort], [CreatedTime], [CreatedUserId], [CreatedUser], [ModifiedTime], [ModifiedUserId], [ModifiedUser], [IsDeleted]) VALUES (1, N'管理员', N'拥有系统管理权限', 0, CAST(N'2022-07-09T15:24:17.5280046' AS DateTime2), 0, NULL, NULL, 0, NULL, 0)
GO
INSERT [dbo].[RoleSecurity] ([RoleId], [SecurityId]) VALUES (1, 1)
GO
INSERT [dbo].[RoleSecurity] ([RoleId], [SecurityId]) VALUES (1, 2)
GO
INSERT [dbo].[RoleSecurity] ([RoleId], [SecurityId]) VALUES (1, 3)
GO
INSERT [dbo].[Security] ([Id], [UniqueCode], [UniqueName], [Sort], [CreatedTime], [CreatedUserId], [CreatedUser], [ModifiedTime], [ModifiedUserId], [ModifiedUser], [IsDeleted]) VALUES (1, N'role', N'角色管理', 0, CAST(N'2022-07-09T15:24:17.5281419' AS DateTime2), 0, NULL, NULL, 0, NULL, 0)
GO
INSERT [dbo].[Security] ([Id], [UniqueCode], [UniqueName], [Sort], [CreatedTime], [CreatedUserId], [CreatedUser], [ModifiedTime], [ModifiedUserId], [ModifiedUser], [IsDeleted]) VALUES (2, N'auth', N'权限管理', 0, CAST(N'2022-07-09T15:24:17.5281423' AS DateTime2), 0, NULL, NULL, 0, NULL, 0)
GO
INSERT [dbo].[Security] ([Id], [UniqueCode], [UniqueName], [Sort], [CreatedTime], [CreatedUserId], [CreatedUser], [ModifiedTime], [ModifiedUserId], [ModifiedUser], [IsDeleted]) VALUES (3, N'employee', N'员工管理', 0, CAST(N'2022-07-09T15:24:17.5281425' AS DateTime2), 0, NULL, NULL, 0, NULL, 0)
GO
INSERT [dbo].[User] ([Id], [Account], [Password], [Name], [Phone], [LinkPost], [Remark], [CreatedTime], [CreatedUserId], [CreatedUser], [ModifiedTime], [ModifiedUserId], [ModifiedUser], [IsDeleted]) VALUES (1, N'admin', N'c33367701511b4f6020ec61ded352059', NULL, NULL, NULL, NULL, CAST(N'2022-07-09T15:24:17.5287194' AS DateTime2), 0, NULL, NULL, 0, NULL, 0)
GO
INSERT [dbo].[UserRole] ([UserId], [RoleId]) VALUES (1, 1)
GO
ALTER TABLE [dbo].[RoleSecurity]  WITH CHECK ADD  CONSTRAINT [FK_RoleSecurity_Role_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[RoleSecurity] CHECK CONSTRAINT [FK_RoleSecurity_Role_RoleId]
GO
ALTER TABLE [dbo].[RoleSecurity]  WITH CHECK ADD  CONSTRAINT [FK_RoleSecurity_Security_SecurityId] FOREIGN KEY([SecurityId])
REFERENCES [dbo].[Security] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[RoleSecurity] CHECK CONSTRAINT [FK_RoleSecurity_Security_SecurityId]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_Role_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_Role_RoleId]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_User_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_User_UserId]
GO
