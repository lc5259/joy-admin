# JoyAdmin
<div align="center"><h1 align="center">JoyAdmin</h1></div>
<div align="center"><h3 align="center">前后端分离架构，开箱即用，紧随前沿技术</h3></div>
#### 介绍
基于Furion和iviewadmin的简单权限、角色、管理后台，使用serilog记录日志
其中iview版本已更新到最新4.7版本

#### 软件架构
前端采用vue,后端采用.net6框架furion
* 个人觉得是目前最好用的.net后台底层框架，文档地址：https://dotnetchina.gitee.io/furion/

极其简洁的角色权限系统，配合sqlserver，适合简单系统和初学者。
小型项目快速搭建首选脚手架。

本人菜鸟一枚，愿共同学习，也欢迎大佬们的鞭挞。
 


 
### 🍄 快速启动

需要安装：VS2022、npm或yarn
* 设置自己的数据库连接，使用codefirst模式,生成初始数据
1. Add-Migration v1.0.0 -Context DefaultDbContext
2. Update-Database
* 也可以使用提供的脚本、或数据库备份文件直接还原
* 启动后台：打开JoyAdminBackend/JoyAdmin/JoyAdmin.sln解决方案，直接运行（F5）即可启动（数据库默认Sqlserver2019）
* 启动前端：打开JoyAdminFrontend文件夹，进行依赖下载，运行npm install或yarn命令，再运行npm run dev或 yarn run dev
* 浏览器访问：`http://localhost:8080` （默认前端端口为：8080，后台端口为：9001）
#### 截图展示
 ![.](https://images.gitee.com/uploads/images/2021/0409/232410_23084faa_1351955.png "111.png")
 
 

 


 
 